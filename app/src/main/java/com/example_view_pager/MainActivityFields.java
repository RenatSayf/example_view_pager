package com.example_view_pager;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Renat.
 */

public class MainActivityFields implements Parcelable
{
    public static final String KEY = "main_activity_fields";
    int oldPosition = 0;

    private MainActivityFields(Parcel in)
    {
    }

    MainActivityFields()
    {

    }

    public static final Creator<MainActivityFields> CREATOR = new Creator<MainActivityFields>()
    {
        @Override
        public MainActivityFields createFromParcel(Parcel in)
        {
            return new MainActivityFields(in);
        }

        @Override
        public MainActivityFields[] newArray(int size)
        {
            return new MainActivityFields[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeInt(oldPosition);
    }
}
