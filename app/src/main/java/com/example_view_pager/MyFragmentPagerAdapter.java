package com.example_view_pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Renat.
 */

public class MyFragmentPagerAdapter extends FragmentPagerAdapter
{
    private String textArg;
    private int position = 0;
    private int oldPosition = 0;
    private Pagination pagination;

    public MyFragmentPagerAdapter(FragmentManager fm, TextView textView, CharSequence mText)
    {
        super(fm);

        pagination = new Pagination(mText,
                textView.getWidth(),
                textView.getHeight(),
                textView.getPaint(),
                textView.getLineSpacingMultiplier(),
                textView.getLineSpacingExtra(),
                textView.getIncludeFontPadding());

        update();
    }

    @Override
    public Fragment getItem(int position)
    {
        this.position = position;
        if (this.position > oldPosition)
        {
            update();
        }

        if (this.position < oldPosition)
        {
            update();
        }
        return PageFragment.newInstance(this.position + 1, textArg);
    }

    @Override
    public int getCount()
    {
        return pagination.size();
    }

    public void update()
    {
        if (pagination != null)
        {
            final CharSequence text = pagination.get(position);
            if (text != null)
            {
                textArg = text.toString();
                oldPosition = position;
            }
        }
    }


}
