package com.example_view_pager;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.Random;



public class PageFragment extends Fragment
{

    private static final String ARG_PAGE_NUMBER = "arg_page_number";
    private static final String ARG_TEXT_CONTENT = "arg_text_content";

    private OnFragmentInteractionListener mListener;

    private String textContent;
    private int pageNumber;
    private int backColor;

    public PageFragment()
    {
        // Required empty public constructor
    }

    public static PageFragment newInstance(int page_number, String text_content)
    {
        PageFragment fragment;
        fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page_number);
        args.putString(ARG_TEXT_CONTENT, text_content);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
            textContent = getArguments().getString(ARG_TEXT_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View fragmentView = inflater.inflate(R.layout.fragment_page, container, false);

        final TextView tvPage = (TextView) fragmentView.findViewById(R.id.tv_book_page);
        Spanned spanned = Html.fromHtml(textContent);
        tvPage.setText(spanned);

        TextView tvPageNumber = (TextView) fragmentView.findViewById(R.id.tv_page_number);
        tvPageNumber.setText(String.valueOf(pageNumber));



//        if (mListener != null)
//        {
//            mListener.onFragmentInteraction(tvPage);
//        }

        return fragmentView;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener
    {
        void onFragmentInteraction(TextView textView);
    }




}
